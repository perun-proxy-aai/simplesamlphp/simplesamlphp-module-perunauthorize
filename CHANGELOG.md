## [2.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perunauthorize/compare/v2.1.0...v2.1.1) (2024-03-08)


### Bug Fixes

* update dependencies ([61d3387](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-perunauthorize/commit/61d33872135ceaceb5b7e25de9d3b7cd27a76e45))

## [v2.1.0](https://github.com/CESNET/perunauthorize-simplesamlphp-module/tree/v2.1.0)

### Added

- Added .gitignore

### Changed

- Double quotes changed to single quotes
- Using of short array syntax (from array() to [])
- Code improvements

## [v2.0.0](https://github.com/CESNET/perunauthorize-simplesamlphp-module/tree/v2.0.0)

### Added

- Added file phpcs.xml

### Changed

- Changed code style to PSR-2
- Module uses namespaces

## [v1.1.1](https://github.com/CESNET/perunauthorize-simplesamlphp-module/tree/v1.1.1)

### Changed

- Changed header in perunauthorize_403.php

## [v1.1.0](https://github.com/CESNET/perunauthorize-simplesamlphp-module/tree/v1.1.0)

### Added

- Czech translation

### Changed

- Classes SimpleSAML_Logger and SimpleSAML_Module renamed to SimpleSAML\Logger and SimpleSAML\Module
- Removed Perunauthorize.translation.json

## [v1.0.0](https://github.com/CESNET/perunauthorize-simplesamlphp-module/tree/v1.0.0)

### Added

- Changelog
