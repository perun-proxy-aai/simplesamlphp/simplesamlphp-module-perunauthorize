<?php

namespace lib;

class TemplateHelper
{
    public function getBaseURL($t, $type = 'get', $key = null, $value = null): string
    {
        if (isset($t->data['informationURL'])) {
            $vars = [
                'informationURL' => $t->data['informationURL'],
                'administrationContact' => $t->data['administrationContact'],
                'serviceName' => $t->data['serviceName'],
            ];
        } else {
            $vars = [
                'administrationContact' => $t->data['administrationContact'],
                'serviceName' => $t->data['serviceName'],
            ];
        }

        if (isset($key)) {
            if (isset($vars[$key])) {
                unset($vars[$key]);
            }
            if (isset($value)) {
                $vars[$key] = $value;
            }
        }

        if ($type === 'get') {
            return 'perunauthorize_403.php?' . http_build_query($vars, '', '&amp;');
        }

        $text = '';
        foreach ($vars as $k => $v) {
            $text .= '<input type="hidden" name="' . $k . '" value="' . htmlspecialchars($v) . '" />' . PHP_EOL;
        }
        return $text;
    }
}
